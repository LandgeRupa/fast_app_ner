# from typing import Union
# from fastapi import FastAPI
# from enum import Enum 
# from pydantic import BaseModel


from fastapi import FastAPI
import spacy
from pydantic import BaseModel

en_core_web = spacy.load("en_core_web_sm")
NER_app = FastAPI(tags=['sentence'])

class Input(BaseModel):
    sentence: str

@NER_app.post("/analyze_text")
def get_text_characteristics(sentence_input: Input):
    document = en_core_web(sentence_input.sentence)
    output_array = []
    for token in document:
        output = {
            "Index": token.i, "Token": token.text, "Tag": token.tag_, "POS": token.pos_,
            "Dependency": token.dep_, "Lemma": token.lemma_, "Shape": token.shape_,
            "Alpha": token.is_alpha, "Is Stop Word": token.is_stop
        }
        output_array.append(output)
    return {"output": output_array}

@NER_app.post("/entity_recognition")
def get_entity(sentence_input: Input):
    document = en_core_web(sentence_input.sentence)
    output_array = []
    for token in document.ents:
        output = {
            "Text": token.text, "Start Char": token.start_char,
            "End Char": token.end_char, "Label": token.label_
        }
        output_array.append(output)
    return {"output": output_array}

     

















# app = FastAPI()


# from enum import Enum

# from fastapi import FastAPI


# class ModelName(str, Enum):
#     alexnet = "alexnet"
#     resnet = "resnet"
#     lenet = "lenet"

# class Item(BaseModel):
#     name : str
#     price: float
#     is_offer : Union[bool, None] = None 


# @app.get("/models/{model_name}")
# async def get_model(model_name: ModelName):
#     if model_name is ModelName.alexnet:
#         return {"model_name": model_name, "message": "Deep Learning FTW!"}

#     if model_name.value == "lenet":
#         return {"model_name": model_name, "message": "LeCNN all the images"}

#     return {"model_name": model_name, "message": "Have some residuals"}




# @app.get("/")
# def read_root():
#     return{"Hello": "World"}

# @app.get("/")
# async def root():
#   return {"message":"Hello World"}

# @app.get("/items/{item_id}")
# def read_item(item_id: int, q:Union[str, None] =None):
#     return{"item_id": item_id, "q":q}

# @app.put("/items/{item_id}")
# def update_item(item_id: int, item:Item):
#     return{"item_name": item.name, "item-id":item_id}

# @app.put("/items/{item_id}")
# def save_item(item_id: int, item:Item):
#     return{"item_name":item.price,"item-id":item_id}

